package network.elekmonar.commons.testing;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SharedData implements Serializable {

	private static final long serialVersionUID = -2094600200705392872L;
	
	private static Map<String, Object> parameters;
	
	static {
		parameters = new HashMap<>();
	}
	
	public static void put(String key, Object value) {
		parameters.put(key, value);
	}
	
	public static Object get(String parameter) {
		return parameters.get(parameter);
	}	

}
