package network.elekmonar.commons.spec.able;

public interface NaturalIdable {

	public String getNaturalId();
	
	public void setNaturalId(String naturalId);
	
}