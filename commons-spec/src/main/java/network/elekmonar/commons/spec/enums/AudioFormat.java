package network.elekmonar.commons.spec.enums;

public enum AudioFormat {

	OGG,
	
	MP3;
	
}