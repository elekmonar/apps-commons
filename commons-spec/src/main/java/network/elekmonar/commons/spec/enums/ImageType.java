package network.elekmonar.commons.spec.enums;

import java.util.HashMap;
import java.util.Map;

public enum ImageType {
	
	PNG("image/png"),
	
	JPG("image/jpeg"),
	
	GIF("image/gif"),
	
	SVG("image/svg");
	
	static Map<String, ImageType> valuesByContentType;
	
	static {
		valuesByContentType = new HashMap<>();
		for (ImageType imageType : ImageType.values()) {
			valuesByContentType.put(imageType.getContentType(), imageType);
		}
	}
	
	String contentType;
	
	ImageType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}
	
	public static ImageType fromContentType(String contentType) {
		return valuesByContentType.get(contentType);
	}

}