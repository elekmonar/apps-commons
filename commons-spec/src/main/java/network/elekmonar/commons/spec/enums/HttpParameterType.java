package network.elekmonar.commons.spec.enums;

/**
 * Тип параметра HTTP-запроса
 * 
 * @author Vitaly Masterov
 * 
 * @since 0.1
 *
 */
public enum HttpParameterType {
	
	QUERY,
	
	HEADER,
	
	FORM;

}