package network.elekmonar.commons.spec.exceptions;

/**
 * Выбрасывается, когда тип идентификатора не поддерживается в исполняемом фрагменте.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class UnsupportedIdentifierTypeException extends IllegalArgumentException {

	private static final long serialVersionUID = 1817918635572388617L;
	
	public UnsupportedIdentifierTypeException() {
		super();
	}

}