package network.elekmonar.commons.spec.dto;

import java.io.Serializable;

public class DataSelectionMeta implements Serializable {
	
	private static final long serialVersionUID = -4943922803421733319L;

	private Long count;
	
	private Integer page;
	
	public DataSelectionMeta() {
	}
	
	public DataSelectionMeta(Long count, Integer page) {
		this.count = count;
		this.page = page;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}	

}