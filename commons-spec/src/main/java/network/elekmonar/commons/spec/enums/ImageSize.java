package network.elekmonar.commons.spec.enums;

public enum ImageSize {
	
	/**
	 * Extra Small
	 */
	XS("xs"),
	
	/**
	 * Small
	 */
	SM("sm"),
	
	/**
	 * Medium
	 */
	MD("md"),
	
	/**
	 * Large
	 */
	LG("lg"),
	
	/**
	 * Extra Lanrge
	 */
	XL("xl");
	
	private String abbr;
	
	ImageSize(String abbr) {
		this.abbr = abbr;
	}

	public String getAbbr() {
		return abbr;
	}
	
}