package network.elekmonar.commons.spec.key;

import java.io.Serializable;

import network.elekmonar.commons.spec.enums.Language;

public class LangStringKey implements Serializable {

	private static final long serialVersionUID = 5838480337885983147L;
	
	private Language first;
	
	private String second;
	
	public LangStringKey(Language first, String second) {
		this.first = first;
		this.second = second;
	}

	public Language getFirst() {
		return first;
	}

	public void setFirst(Language first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}
	
	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof LangStringKey)) {
            return false;
        }
        LangStringKey obj = (LangStringKey) value;

        if (getFirst() == null || obj.getFirst() == null
        		|| getSecond() == null || obj.getSecond() == null) {
        	return false;
        }        
        
        return obj.getFirst().equals(getFirst()) && obj.getSecond().equals(getSecond());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getFirst() != null ? getFirst().hashCode() : 0);
        result += 29 * (getSecond() != null ? getSecond().hashCode() : 0);
        return result;
    }

}