package network.elekmonar.commons.spec.events;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Событие, которое возникает по окончании загрузки файла 
 * или группы файлов от клиента на сервер.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class UploadFinishEvent implements Serializable {

	private static final long serialVersionUID = 4019936612991978726L;
	
	/**
	 * Перечень путей только что загруженных на сервер файлов
	 */
	private Path[] filePaths;
	
	public UploadFinishEvent(Path[] filePaths) {
		this.filePaths = filePaths;
	}

	public Path[] getFilePaths() {
		return filePaths;
	}

	public void setFilePaths(Path[] filePaths) {
		this.filePaths = filePaths;
	}

}