package network.elekmonar.commons.spec.enums;

public enum RuntimeContext {
	
	FACES,
	
	REST;

}