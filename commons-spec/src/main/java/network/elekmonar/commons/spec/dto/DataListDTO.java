package network.elekmonar.commons.spec.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class DataListDTO<T> implements Serializable {

	private static final long serialVersionUID = -2687128791336931891L;
	
	private List<T> data;
	
	private DataSelectionMeta meta;
	
	public DataListDTO(List<T> data) {
		if (data != null) {
			this.data = data;
		} else {
			this.data = Collections.emptyList();
		}
		this.meta = new DataSelectionMeta();
	}

	public DataListDTO(List<T> data, DataSelectionMeta meta) {
		if (data != null) {
			this.data = data;
		} else {
			this.data = Collections.emptyList();
		}
		this.meta = meta;
	}
	
	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public DataSelectionMeta getMeta() {
		return meta;
	}

	public void setMeta(DataSelectionMeta meta) {
		this.meta = meta;
	}
	
}
