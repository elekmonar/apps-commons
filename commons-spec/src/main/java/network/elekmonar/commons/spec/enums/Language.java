package network.elekmonar.commons.spec.enums;

public enum Language {
	
	RU("Русский"),
	
	UA("Український"),
	
	KZ("Қазақ"),
	
	EN("English"),
	
	FR("Français"),
	
	DE("Deutsche"),
	
	IT("italiano"),
	
	ES("Español");
	
	private String nativeName;
	
	Language(String nativeName) {
		this.nativeName = nativeName;
	}

	public String getNativeName() {
		return nativeName;
	}

}
