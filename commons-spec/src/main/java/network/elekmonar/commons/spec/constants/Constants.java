package network.elekmonar.commons.spec.constants;

import javax.ws.rs.core.MediaType;

public interface Constants {
	
	/**
	 * JSON UTF-8
	 */
	public static final String UTF8 = "UTF-8";
	
	/**
	 * JSON UTF-8
	 */
	public static final String JSON_UTF8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";	

}