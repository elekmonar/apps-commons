package network.elekmonar.commons.spec.enums;

public enum ContentType {
	
	JAVASCRIPT("js"),
	
	HTML("html"),
	
	XHTML("xhtml"),
	
	CSS("css"),
	
	JSON("json"),
	
	XML("xml"),
	
	JAVA("java");
	
	private String fileExtention;
	
	ContentType(String fileExtention) {
		this.fileExtention = fileExtention;
	}

	public String getFileExtention() {
		return fileExtention;
	}

}
