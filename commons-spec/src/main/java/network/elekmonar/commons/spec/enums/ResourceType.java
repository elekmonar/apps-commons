package network.elekmonar.commons.spec.enums;

public enum ResourceType {
	
	RESOURCE,
	
	INSTANCE_SET;

}