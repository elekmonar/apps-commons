package network.elekmonar.commons.spec.exceptions;

/**
 * Исключение, которое возникает когда не найдена 
 * реализация процесса импорта заданного ресурса.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class NoImportProcessException extends Exception {

	private static final long serialVersionUID = 1289808828076810819L;
	
	private String resource;
	
	public NoImportProcessException(String resource) {
		this.resource = resource;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

}