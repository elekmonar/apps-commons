package network.elekmonar.commons.spec.msg;

import java.io.Serializable;

public class NotificationMsg implements Serializable {

	private static final long serialVersionUID = 4741613987915791038L;
	
	private String time;
	
	private String from;
	
	private String to;
	
	private String token;
	
	private String body;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}