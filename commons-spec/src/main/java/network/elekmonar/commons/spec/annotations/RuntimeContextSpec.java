package network.elekmonar.commons.spec.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

import network.elekmonar.commons.spec.enums.RuntimeContext;

@Target(value={java.lang.annotation.ElementType.TYPE,java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.PARAMETER})
@Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@Documented
@Qualifier
public @interface RuntimeContextSpec {
	
	RuntimeContext value();

}