package network.elekmonar.commons.spec.events;

import java.io.Serializable;
import java.util.Map;

/**
 * Событие, сигнализирующее о том, что необходимо
 * запустить процесс импорта заданного ресурса
 * из файлов в базу данных.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ImportStartEvent implements Serializable {

	private static final long serialVersionUID = 7664339583836352118L;
	
	private Map<String, String> payload;
	
	public ImportStartEvent() {
		
	}
	
	public ImportStartEvent(Map<String, String> payload) {
		this.payload = payload;
	}
	
	public Map<String, String> getPayload() {
		return payload;
	}

	public void setPayload(Map<String, String> payload) {
		this.payload = payload;
	}
	
}