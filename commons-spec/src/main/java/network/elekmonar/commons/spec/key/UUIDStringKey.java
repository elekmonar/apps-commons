package network.elekmonar.commons.spec.key;

import java.io.Serializable;
import java.util.UUID;

public class UUIDStringKey implements Serializable {

	private static final long serialVersionUID = -2510658930898460599L;
	
	private UUID first;
	
	private String second;
	
	public UUIDStringKey(UUID first, String second) {
		this.first = first;
		this.second = second;
	}

	public UUID getFirst() {
		return first;
	}

	public void setFirst(UUID first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}
	
	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof UUIDStringKey)) {
            return false;
        }
        UUIDStringKey obj = (UUIDStringKey) value;

        if (getFirst() == null || obj.getFirst() == null
        		|| getSecond() == null || obj.getSecond() == null) {
        	return false;
        }        
        
        return obj.getFirst().equals(getFirst()) && obj.getSecond().equals(getSecond());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getFirst() != null ? getFirst().hashCode() : 0);
        result += 29 * (getSecond() != null ? getSecond().hashCode() : 0);
        return result;
    }

}