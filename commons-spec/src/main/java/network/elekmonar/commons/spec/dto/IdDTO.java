package network.elekmonar.commons.spec.dto;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;

public class IdDTO implements Serializable {

	private static final long serialVersionUID = -7815412748342694061L;
		
	private UUID id;
	
	public IdDTO() {
		
	}

	public IdDTO(UUID id) {
		this.id = id;
	}
	
	@JsonProperty("id")
	@JsonSerialize(using = UUIDSerializer.class)
	@JsonDeserialize(using = UUIDDeserializer.class)		
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

}
