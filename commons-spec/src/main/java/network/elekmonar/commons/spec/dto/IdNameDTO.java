package network.elekmonar.commons.spec.dto;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IdNameDTO implements Serializable {

	private static final long serialVersionUID = -3777305430879846627L;
	
	private UUID id;
	
	private String name;
	
	public IdNameDTO() {
		
	}
	
	public IdNameDTO(UUID id, String name) {
		this.id = id;
		this.name = name;
	}	

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

}
