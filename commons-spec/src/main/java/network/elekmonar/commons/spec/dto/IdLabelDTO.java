package network.elekmonar.commons.spec.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class IdLabelDTO implements Serializable {

	private static final long serialVersionUID = -5138891223844160347L;
	
	private UUID id;
	
	private String label;

    public IdLabelDTO() {
    }

    public IdLabelDTO(UUID id, String label) {
		this.id = id;
		this.label = label;
	}

    @NotNull
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
