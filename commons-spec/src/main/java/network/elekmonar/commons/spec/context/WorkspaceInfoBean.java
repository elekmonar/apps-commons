package network.elekmonar.commons.spec.context;

import java.io.Serializable;

import javax.enterprise.inject.Vetoed;

import network.elekmonar.modular.spec.workspace.WorkspaceInfo;

@Vetoed
public class WorkspaceInfoBean implements WorkspaceInfo, Serializable {

	private static final long serialVersionUID = 497429532641300825L;
	
	private Integer id;
	
	private String name;
	
	private String lang;

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	
}