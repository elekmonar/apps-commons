package network.elekmonar.commons.spec.enums;

import java.util.Map;

public enum RepresentationFormat {	
	
	JSON("application/json"),
	
	JSON_API("application/vnd.api+json"),
	
	XML("application/xml");
	
	private static Map<String, RepresentationFormat> formatsByMimeTypes;
	
	private static final String ERR_MSG = "Invalid MIME Type '%s' for enum RepresentationFormat";
	
	static {
		formatsByMimeTypes = Map.of(
				JSON.getContentType(), JSON,
				JSON_API.getContentType(), JSON_API,
				XML.getContentType(), XML
		);
	}
	
	private String contentType;
	
	private RepresentationFormat(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}
	
	public static RepresentationFormat fromMimeType(String mimeType) {
		RepresentationFormat rf = formatsByMimeTypes.get(mimeType);
		if (rf == null) {
			throw new IllegalArgumentException(String.format(ERR_MSG, mimeType));
		}
		
		return rf;
	}
	
}