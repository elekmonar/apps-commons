package network.elekmonar.commons.spec.exceptions;

import java.io.Serializable;

/**
 * Наиболее общее исключение {@literal runtime}-уровня.
 * Используется для того, чтобы пробросить наверх
 * завёрнутое в него не {@literal runtime}-исключение.
 * У каждой программной реализации (JSF, REST, EJB) своя
 * "точка отлова" этого исключения, в которой находится соответствующий 
 * программной реализции обработчик. 
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ElekmonarException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 374750449929020690L;
	
	public ElekmonarException(Exception e) {
		super();
	}

}
