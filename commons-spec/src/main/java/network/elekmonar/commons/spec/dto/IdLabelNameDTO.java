package network.elekmonar.commons.spec.dto;

import java.io.Serializable;
import java.util.UUID;

public class IdLabelNameDTO implements Serializable {

	private static final long serialVersionUID = -3249352612919809695L;

	private UUID id;
	
	private String label;
	
	private String name;
	
	public IdLabelNameDTO() {
		
	}
	
	public IdLabelNameDTO(UUID id, String label, String name) {
		this.id = id;
		this.label = label;
		this.name = name;
	}	

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
