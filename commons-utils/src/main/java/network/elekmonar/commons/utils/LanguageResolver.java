package network.elekmonar.commons.utils;

import java.io.Serializable;

import javax.inject.Inject;

import org.apache.deltaspike.core.api.config.ConfigProperty;

import network.elekmonar.commons.spec.enums.Language;

public class LanguageResolver implements Serializable {

	private static final long serialVersionUID = -5129319589053926472L;
	
	@Inject @ConfigProperty(name = "default.language", defaultValue = "ru")
	private String defaultLanguage;
	
	public Language resolve(String acceptLanguage) {
		if (acceptLanguage == null) {
			return Language.valueOf(defaultLanguage.toUpperCase());
		} else {
			if (acceptLanguage.length() == 2) {
				return Language.valueOf(acceptLanguage.toUpperCase());	
			} else {
				return Language.valueOf(acceptLanguage.substring(0, 2).toUpperCase());
			}			
		}
	}

}