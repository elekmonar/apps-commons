package network.elekmonar.commons.utils;

import java.io.Serializable;

import network.elekmonar.commons.spec.enums.RepresentationFormat;

public class RepresentationFormatResolver implements Serializable {

	private static final long serialVersionUID = -7835649973140871696L;
	
	public RepresentationFormat resolve(String accept) {
		RepresentationFormat rf = null;
		if (accept == null || accept.equals("*/*")) {
			rf = RepresentationFormat.JSON;
		}
		
		if (rf == null) {
			rf = RepresentationFormat.fromMimeType(accept);
		}
		
		return rf;
	}

}