package network.elekmonar.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
//import javax.ws.rs.WebApplicationException;

import org.apache.commons.codec.digest.DigestUtils;

import network.elekmonar.commons.spec.able.NaturalIdable;

public class ChecksumHelper implements Serializable {

	private static final long serialVersionUID = -7125712452431425953L;
	
	private static final String NATURAL_ID = "naturalId";
	
	private static final String CHECKSUM = "checksum";

	@PersistenceContext
	private EntityManager em;
	
	public String calculate(String... args) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			sb.append(args[i]);
			sb.append(".");
		}
		
		sb.deleteCharAt(sb.length()-1);
		
		return DigestUtils.sha256Hex(sb.toString().getBytes());
	}

	public <T extends NaturalIdable> Map<String, String> loadFromDatabase(Class<T> entityClass) {
		Map<String, String> checksums = new HashMap<>();
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> cQuery = cBuilder.createTupleQuery();
		Root<T> cRoot = cQuery.from(entityClass);		
		cQuery.multiselect(cRoot.get(NATURAL_ID), cRoot.get(CHECKSUM));
		List<Tuple> tuples = em.createQuery(cQuery).getResultList();
		for (Tuple tuple : tuples) {
			checksums.put(tuple.get(0, String.class), tuple.get(1, String.class));
		}
		
		return checksums;
	}

	public String calculate(String strValue) {
		return DigestUtils.sha256Hex(strValue.getBytes());
	}
	
	/**
	 * Вычисление хэша по содержимому файлы, либо группы файлов,
	 * в зависимости от того, на что указывает параметр {@literal path}.
	 * Если в качестве параметра передаётся директория, то
	 * 
	 * находящихся внутри заданного каталога. Для вычисления
	 * используется рекурсивный обход каталога со всеми 
	 * внутренними подкаталогами и файлами. 
	 * 
	 * @param path
	 * @return
	 */
	public String calculate(Path path) {
		if (Files.isDirectory(path)) {
			return calculateDirectoryHash(path);
		} else {
			return calculateSingleFileHash(path);
		}
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public String calculate(File file) {
		if (file.isDirectory()) {
			return calculateDirectoryHash(file);
		} else {
			return calculateSingleFileHash(file);
		}		
	}
	
	/**
	 * Вычисление хэша по содержимому одного заданного файла
	 * 
	 * @param filePath путь к директории в виде объекта {@link Path}
	 * @return строка, содержащая значение хэша
	 */
	private String calculateSingleFileHash(Path filePath) {
		return calculateSingleFileHash(filePath.toFile());
	}
	
	/**
	 * Вычисление хэша по содержимому группы файлов,
	 * находящихся внутри заданного каталога. Для вычисления
	 * используется рекурсивный обход каталога со всеми 
	 * внутренними подкаталогами и файлами.
	 * 
	 * @param filePath путь к файлу в виде объекта {@link Path}
	 * @return строка, содержащая значение хэша
	 */
	private String calculateSingleFileHash(File file) {
		try (
				InputStream is = new FileInputStream(file)
		) {
			return DigestUtils.sha256Hex(is);	
		} catch(IOException e) {
//			throw new WebApplicationException(e);
			throw new RuntimeException(e);
		}		
	}

	/**
	 * Вычисление хэша по содержимому группы файлов,
	 * находящихся внутри заданного каталога. Для вычисления
	 * используется рекурсивный обход каталога со всеми 
	 * внутренними подкаталогами и файлами.
	 * 
	 * @param dirFile путь к файлу в виде объекта {@link File}
	 * @return строка, содержащая значение хэша
	 */
	private String calculateDirectoryHash(File dirFile) {
		return calculateDirectoryHash(dirFile.toPath());
	}
	
	/**
	 * Вычисление хэша по содержимому группы файлов,
	 * находящихся внутри заданного каталога. Для вычисления
	 * используется рекурсивный обход каталога со всеми 
	 * внутренними подкаталогами и файлами.
	 * 
	 * @param dirPath путь к файлу в виде объекта {@link Path}
	 * @return строка, содержащая значение хэша
	 */
	private String calculateDirectoryHash(Path dirPath) {
		try {
			StringBuilder sb = new StringBuilder();
			Files.walk(dirPath)
			  .sorted(Comparator.reverseOrder())
			  .filter(Files::isRegularFile)
			  .map(Path::toFile)
			  .forEach(file -> {				  
				  try (InputStream is = new FileInputStream(file)) {
					  sb.append(DigestUtils.sha256Hex(is));
				  } catch(IOException e) {
					  //throw new WebApplicationException(e);
					  throw new RuntimeException(e);
				  }
			  });
			
			return DigestUtils.sha256Hex(sb.toString().getBytes());
		} catch(IOException e) {
//			throw new WebApplicationException(e);
			throw new RuntimeException(e);
		}
	}
	
}