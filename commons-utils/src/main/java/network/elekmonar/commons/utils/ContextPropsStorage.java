package network.elekmonar.commons.utils;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;

import network.elekmonar.commons.spec.key.UUIDStringKey;

@ApplicationScoped
public class ContextPropsStorage implements Serializable {

	private static final long serialVersionUID = -2825157447189809947L;
	
	private Map<UUIDStringKey, Object> map;
	
	public ContextPropsStorage() {
		map = new ConcurrentHashMap<UUIDStringKey, Object>();
	}
	
	public Object getValue(UUID firstKey, String secondKey) {
		UUIDStringKey key = new UUIDStringKey(firstKey, secondKey);
		return map.remove(key);
	}
	
	public void setValue(UUID firstKey, String secondKey, Object value) {
		UUIDStringKey key = new UUIDStringKey(firstKey, secondKey);
		map.put(key, value);
	}

}