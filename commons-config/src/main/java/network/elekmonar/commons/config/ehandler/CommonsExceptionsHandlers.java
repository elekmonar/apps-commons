package network.elekmonar.commons.config.ehandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.enterprise.event.Observes;

import org.apache.deltaspike.core.api.exception.control.ExceptionHandler;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionEvent;

@ExceptionHandler
public class CommonsExceptionsHandlers {

	public void handleIOException(@Observes ExceptionEvent<IOException> event) {
		
	}

	public void handleIllegalAccessException(@Observes ExceptionEvent<IllegalAccessException> event) {
		
	}

	public void handleIllegalArgumentException(@Observes ExceptionEvent<IllegalArgumentException> event) {
		
	}
	
	public void handleInvocationTargetException(@Observes ExceptionEvent<InvocationTargetException> event) {
		
	}

	public void handleNoSuchMethodException(@Observes ExceptionEvent<NoSuchMethodException> event) {
		
	}

	public void handleSecurityException(@Observes ExceptionEvent<SecurityException> event) {
		
	}

	public void handleSqlExceptionException(@Observes ExceptionEvent<SQLException> event) {
		System.out.println("[handleSqlExceptionException] msg = " + event.getException().getMessage());
	}
	
}