package network.elekmonar.commons.impl.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import network.elekmonar.commons.impl.IdNameFactoryImpl;
import network.elekmonar.commons.impl.beans.IntegerIdNameBean;
import network.elekmonar.commons.impl.beans.LongIdNameBean;
import network.elekmonar.commons.impl.beans.ShortIdNameBean;
import network.elekmonar.commons.spec.exceptions.UnsupportedIdentifierTypeException;

public class IdFactoryImplTester {
	
	@Test
	public void testNewInstance() {
		IdNameFactoryImpl factory = new IdNameFactoryImpl();

		Assert.assertTrue(factory.newInstance(Long.class) instanceof LongIdNameBean);
		Assert.assertTrue(factory.newInstance(Integer.class) instanceof IntegerIdNameBean);
		Assert.assertTrue(factory.newInstance(Short.class) instanceof ShortIdNameBean);
		
		Assert.assertThrows(UnsupportedIdentifierTypeException.class, () -> factory.newInstance(String.class));		
	}

}