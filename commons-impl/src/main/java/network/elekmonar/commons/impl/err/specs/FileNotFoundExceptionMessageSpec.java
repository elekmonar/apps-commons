package network.elekmonar.commons.impl.err.specs;

import java.io.FileNotFoundException;
import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link FileNotFoundException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see FileNotFoundException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(FileNotFoundException.class)
public class FileNotFoundExceptionMessageSpec implements SystemErrorMessageSpec<FileNotFoundException>, Serializable {

	private static final long serialVersionUID = -265115597092837326L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(FileNotFoundException e) {
        SystemErrorMessage msg = new SystemErrorMessage();
        msg.setStatus(404);
        msg.setCode(204);
        msg.setTitle(messages.fileNotFoundExceptionTitle());
        msg.setDetail(messages.fileNotFoundExceptionDetail(e.getMessage()));
        return msg;
	}

}