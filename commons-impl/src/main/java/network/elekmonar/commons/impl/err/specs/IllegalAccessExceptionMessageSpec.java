package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link IllegalAccessException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see IllegalAccessException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(IllegalAccessException.class)
public class IllegalAccessExceptionMessageSpec implements SystemErrorMessageSpec<IllegalAccessException>, Serializable {

	private static final long serialVersionUID = -1939877027101273710L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(IllegalAccessException e) {
		// TODO Auto-generated method stub
		return null;
	}

}