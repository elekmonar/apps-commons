package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link NoSuchMethodException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see NoSuchMethodException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(NoSuchMethodException.class)
public class NoSuchMethodExceptionMessageSpec implements SystemErrorMessageSpec<NoSuchMethodException>, Serializable {

	private static final long serialVersionUID = 8687529307251729650L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(NoSuchMethodException e) {
		// TODO Auto-generated method stub
		return null;
	}

}