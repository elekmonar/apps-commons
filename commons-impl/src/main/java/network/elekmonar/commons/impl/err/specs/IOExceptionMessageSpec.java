package network.elekmonar.commons.impl.err.specs;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link IOException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see IOException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(IOException.class)
public class IOExceptionMessageSpec implements SystemErrorMessageSpec<IOException>, Serializable {

	private static final long serialVersionUID = -6682861027601205575L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(IOException e) {
        SystemErrorMessage msg = new SystemErrorMessage();
        msg.setStatus(501);
        msg.setCode(204);
        msg.setTitle(messages.ioExceptionTitle());
        msg.setDetail(messages.ioExceptionDetail());
        return msg;
	}

}