package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link InstantiationException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see InstantiationException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(InstantiationException.class)
public class InstantiationExceptionMessageSpec implements SystemErrorMessageSpec<InstantiationException>, Serializable {

	private static final long serialVersionUID = 5427025283877968892L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(InstantiationException e) {
		// TODO Auto-generated method stub
		return null;
	}

}