package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link IllegalArgumentException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see IllegalArgumentException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(IllegalArgumentException.class)
public class IllegalArgumentExceptionMessageSpec implements SystemErrorMessageSpec<IllegalArgumentException>, Serializable {

	private static final long serialVersionUID = -1370582830077008482L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(IllegalArgumentException e) {
		// TODO Auto-generated method stub
		return null;
	}

}