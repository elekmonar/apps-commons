package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.exceptions.NoRequiredParameterException;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

@HandlerFor(NoRequiredParameterException.class)
public class NoRequiredParameterMessageSpec implements SystemErrorMessageSpec<NoRequiredParameterException>, Serializable {

	private static final long serialVersionUID = -7686493285216426183L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(NoRequiredParameterException e) {
        SystemErrorMessage msg = new SystemErrorMessage();
        msg.setStatus(404);
        msg.setCode(211);
        msg.setTitle(messages.noRequiredParameterTitle());
        msg.setDetail(messages.noRequiredParameterDetail(e.getLabel()));
        return msg;
	}

}
