package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link SecurityException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see SecurityException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(SecurityException.class)
public class SecurityExceptionMessageSpec implements SystemErrorMessageSpec<SecurityException>, Serializable {

	private static final long serialVersionUID = 4977790007236422293L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(SecurityException e) {
		// TODO Auto-generated method stub
		return null;
	}

}