package network.elekmonar.commons.impl;

import java.io.Serializable;

import network.elekmonar.commons.impl.beans.IntegerIdNameBean;
import network.elekmonar.commons.impl.beans.LongIdNameBean;
import network.elekmonar.commons.impl.beans.ShortIdNameBean;
import network.elekmonar.commons.spec.exceptions.UnsupportedIdentifierTypeException;
import network.elekmonar.modular.spec.IdName;
import network.elekmonar.modular.spec.IdNameFactory;

/**
 * Имплементация фабрики по созданию разнотипных объетов {@literal IdName}
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class IdNameFactoryImpl implements IdNameFactory, Serializable {

	private static final long serialVersionUID = 3466435909261715052L;

	@Override
	@SuppressWarnings("unchecked")
	public <T> IdName<T> newInstance(Class<T> idType) {
		if (idType.equals(Long.class)) {
			return (IdName<T>)(new LongIdNameBean());
		} else if (idType.equals(Integer.class)) {
			return (IdName<T>)(new IntegerIdNameBean());
		} else if (idType.equals(Short.class)) {
			return (IdName<T>)(new ShortIdNameBean());
		}
		
		throw new UnsupportedIdentifierTypeException();
	}

}