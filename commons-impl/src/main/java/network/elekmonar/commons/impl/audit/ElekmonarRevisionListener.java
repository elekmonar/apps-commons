package network.elekmonar.commons.impl.audit;

import static network.elekmonar.commons.spec.enums.RuntimeContext.FACES;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.http.HttpServletRequest;

import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;
import org.hibernate.envers.RevisionListener;

import network.elekmonar.commons.persistence.audit.RevisionInfo;
import network.elekmonar.commons.spec.annotations.RuntimeContextSpec;
import network.elekmonar.modular.spec.workspace.WorkspaceInfo;

/**
 * Имплементация {@link RevisionListener}, в которой
 * добавляется идентифкатор пользователя, совершившего операцию.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see RevisionListener
 * @see RevisionInfo
 * @see KeycloakSecurityContext
 * @see KeycloakPrincipal
 *
 */
public class ElekmonarRevisionListener implements RevisionListener, Serializable {

	private static final long serialVersionUID = 4688183933388990087L;

	@Override
    @SuppressWarnings("unchecked")
    public void newRevision(Object revisionEntity) {		
		RevisionInfo revisionInfo = (RevisionInfo)revisionEntity;
		
		Annotation ann = AnnotationInstanceProvider.of(RuntimeContextSpec.class, Map.of("value", FACES));
		WorkspaceInfo workspaceInfo = CDI.current().select(WorkspaceInfo.class, ann).get();
		
		HttpServletRequest httpRequest = BeanProvider.getContextualReference(HttpServletRequest.class, false);		
		/*
		UserBean userBean = BeanProvider.getContextualReference(UserBean.class, false);
            UUID workspaceId = (UUID)userBean.getWindowProperty(CURRENT_WORKSPACE);

            KeycloakPrincipal<KeycloakSecurityContext> principal = (KeycloakPrincipal<KeycloakSecurityContext>)httpRequest.getUserPrincipal();
            UUID userId = UUID.fromString(principal.getName());

            revisionInfo.setUserId(userId);
            revisionInfo.setWorkspaceId(workspaceId);
		*/
		revisionInfo.setWorkspaceId(workspaceInfo.getId());
		revisionInfo.setUserId(UUID.randomUUID());
    }
	
}