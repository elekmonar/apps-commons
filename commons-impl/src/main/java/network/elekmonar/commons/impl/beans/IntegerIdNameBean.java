package network.elekmonar.commons.impl.beans;

import java.io.Serializable;

import network.elekmonar.modular.spec.IdName;

public class IntegerIdNameBean implements IdName<Integer>, Serializable {

	private static final long serialVersionUID = 5148867711217843950L;

	private Integer id;
	
	private String name;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof IntegerIdNameBean)) {
            return false;
        }
        IntegerIdNameBean obj = (IntegerIdNameBean) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}