package network.elekmonar.commons.impl.beans;

import java.io.Serializable;

import network.elekmonar.modular.spec.IdName;

public class LongIdNameBean implements IdName<Long>, Serializable {
	
	private static final long serialVersionUID = 6317672294252994118L;

	private Long id;
	
	private String name;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof LongIdNameBean)) {
            return false;
        }
        LongIdNameBean obj = (LongIdNameBean) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}