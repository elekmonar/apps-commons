package network.elekmonar.commons.impl.workspace;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import network.elekmonar.commons.spec.annotations.RuntimeContextSpec;
import network.elekmonar.commons.spec.context.WorkspaceInfoBean;
import network.elekmonar.commons.spec.enums.RuntimeContext;
import network.elekmonar.modular.spec.workspace.WorkspaceInfo;
import network.elekmonar.modular.spec.workspace.WorkspaceInfoProvider;

public class WorkspaceInfoRestProducer implements Serializable {

	private static final long serialVersionUID = -2980577836019737369L;
	
	private static final String ACCESS_TOKEN = "access_token";
	
	private static final String LOOKUP_PATH = "java:jboss/exported/workspace-app/workspace-ejb/WorkspaceInfoProviderImpl!network.elekmonar.modular.spec.workspace.WorkspaceInfoProvider";
	
//	@EJB(lookup = "java:jboss/exported/workspace-app/workspace-ejb/WorkspaceInfoProviderImpl!network.elekmonar.modular.spec.workspace.WorkspaceInfoProvider")
	private WorkspaceInfoProvider workspaceProvider;
	
	@Produces @RuntimeContextSpec(RuntimeContext.REST)
	public WorkspaceInfoBean produce() {
		HttpServletRequest httpRequest = CDI.current().select(HttpServletRequest.class).get();
		String accessToken = httpRequest.getParameter(ACCESS_TOKEN);
		
		try {
			Context ctx = new InitialContext();
			WorkspaceInfoProvider workspaceProvider = (WorkspaceInfoProvider)ctx.lookup(LOOKUP_PATH);
		} catch (NamingException e) {
			// TODO !!!
			throw new RuntimeException(e);
		}		
		
		WorkspaceInfo workspace = workspaceProvider.getWorkspaceInfo(accessToken);
		return (WorkspaceInfoBean)workspace;
	}		
	
}