package network.elekmonar.commons.impl.beans;

import java.io.Serializable;

import network.elekmonar.modular.spec.IdName;

public class ShortIdNameBean implements IdName<Short>, Serializable {

	private static final long serialVersionUID = -7956310878101312415L;

	private Short id;
	
	private String name;

	@Override
	public Short getId() {
		return id;
	}

	@Override
	public void setId(Short id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof ShortIdNameBean)) {
            return false;
        }
        ShortIdNameBean obj = (ShortIdNameBean) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}