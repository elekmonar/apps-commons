package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import javax.inject.Inject;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link InvocationTargetException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see InvocationTargetException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(InvocationTargetException.class)
public class InvocationTargetExceptionMessageSpec implements SystemErrorMessageSpec<InvocationTargetException>, Serializable {

	private static final long serialVersionUID = 4341876878931634926L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(InvocationTargetException e) {
		// TODO Auto-generated method stub
		return null;
	}

}