package network.elekmonar.commons.impl.err.specs;

import java.io.Serializable;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;

import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.messages.ErrorMessages;
import network.elekmonar.libs.commons.messages.SystemErrorMessage;
import network.elekmonar.libs.commons.spi.SystemErrorMessageSpec;

/**
 * Формирование объекта ошибки для исключения {@link JsonParseException}. 
 * 
 * @author Vitaly Masterov
 * @since 0.6
 * @see JsonParseException
 * @see SystemErrorMessageSpec
 * @see HandlerFor
 * @see ErrorMessages
 *
 */
@HandlerFor(JsonParseException.class)
public class JsonParseExceptionMessageSpec implements SystemErrorMessageSpec<JsonParseException>, Serializable {

	private static final long serialVersionUID = -167499917203939101L;

    @Inject
    private ErrorMessages messages;
	
	@Override
	public SystemErrorMessage produce(JsonParseException e) {
        SystemErrorMessage msg = new SystemErrorMessage();
        msg.setStatus(422);
        msg.setCode(205);
        msg.setTitle(messages.jsonParseExceptionTitle());
        msg.setDetail(messages.jsonParseExceptionDetail());
        return msg;
	}

}