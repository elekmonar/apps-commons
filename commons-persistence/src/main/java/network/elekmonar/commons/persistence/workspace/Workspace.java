package network.elekmonar.commons.persistence.workspace;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import network.elekmonar.commons.persistence.usertype.LanguageUserType;
import network.elekmonar.commons.spec.enums.Language;

/**
 * Сущность Workspace только для чтения.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Immutable
@Entity(name = "Workspace")
@Table(
		name = "workspace", 
		schema = "workspace"
)
@TypeDefs({
    @TypeDef(name = "LanguageUserType", typeClass = LanguageUserType.class)
})
public class Workspace implements Serializable {

	private static final long serialVersionUID = 6993085232800099515L;

	/**
	 * Идентификатор рабочего пространства
	 */
	private Integer id;

	/**
	 * Уникальный идентификатор пространства,
	 * использующийся в Keycloak
	 */
	private UUID globalIdentifier;

	/**
	 * Никальная метка/квалификатор рабочего пространства
	 */
	private String label;
	
	/**
	 * Наименование рабочего пространства
	 */
	private String name;
	
	/**
	 * Небольшое произвольное описание
	 */
	private String description;
	
	/**
	 * Язык по умолчанию для этого пространства
	 */
	private Language defaultLanguage;	

	@Id
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "global_identifier")
	public UUID getGlobalIdentifier() {
		return globalIdentifier;
	}

	public void setGlobalIdentifier(UUID globalIdentifier) {
		this.globalIdentifier = globalIdentifier;
	}

	@Column(name = "label")
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Type(type = "LanguageUserType")
	@Column(name = "default_language", columnDefinition = "language")
	public Language getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(Language defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof Workspace)) {
            return false;
        }
        Workspace obj = (Workspace) value;

        if (getGlobalIdentifier() == null || obj.getGlobalIdentifier() == null) {
        	return false;
        }        
        
        return obj.getGlobalIdentifier().equals(getGlobalIdentifier());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        result += 29 * (getGlobalIdentifier() != null ? getGlobalIdentifier().hashCode() : 0);
        return result;
    }

}