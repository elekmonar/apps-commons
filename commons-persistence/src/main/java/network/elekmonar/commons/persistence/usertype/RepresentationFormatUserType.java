package network.elekmonar.commons.persistence.usertype;

import network.elekmonar.commons.spec.enums.RepresentationFormat;
import network.elekmonar.elefante.hibernate.usertype.EnumUserType;

public class RepresentationFormatUserType extends EnumUserType<RepresentationFormat> {

	public RepresentationFormatUserType() {
		super(RepresentationFormat.class);
	}
	
	public RepresentationFormatUserType(Class<RepresentationFormat> clazz) {
		super(clazz);
	}

}