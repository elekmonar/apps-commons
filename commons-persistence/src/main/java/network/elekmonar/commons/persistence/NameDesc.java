package network.elekmonar.commons.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NameDesc implements Serializable {

	private static final long serialVersionUID = 7097318784737309253L;
	
	private String name;
	
	private String description;

	@Column(name = "name", length = 64, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 1024, nullable = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

}