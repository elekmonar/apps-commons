package network.elekmonar.commons.persistence.usertype;

import network.elekmonar.commons.spec.enums.HttpParameterType;
import network.elekmonar.elefante.hibernate.usertype.EnumUserType;

public class HttpParameterTypeUserType extends EnumUserType<HttpParameterType> {
	
	public HttpParameterTypeUserType() {
		super(HttpParameterType.class);
	}

	public HttpParameterTypeUserType(Class<HttpParameterType> clazz) {
		super(clazz);
	}

}