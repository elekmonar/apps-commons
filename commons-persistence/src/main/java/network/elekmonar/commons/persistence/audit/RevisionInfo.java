package network.elekmonar.commons.persistence.audit;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

/**
 * Информация о ревизии, которую логирует {@literal Hibernate Envers}
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Entity(name = "RevisionInfo")
@Table(schema = "audit", name = "revision_info")
@RevisionEntity
public class RevisionInfo implements Serializable {

	private static final long serialVersionUID = 2184186340068501349L;

	/**
	 * Номер ревизии (поле с первичным ключом)
	 */
	private Long revisionNumber;

	/**
	 * Дата и время ревизии
	 */
	private Date revisionTimestamp;

	/**
	 * Идентификатор пользователя, который расположен в базе данных {@literal Keycloak}.
	 */
	private UUID userId;

	/**
	 * Идентификатор рабочего пространства, в котором находился пользователь в
	 * момент совершения операции.
	 */
	private Integer workspaceId;

	private Set<String> changedEntities;

	@Id
	@GeneratedValue
	@RevisionNumber
	@Column(name = "revision_number", nullable = false)
	public Long getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Long revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	@RevisionTimestamp
	@Column(name = "revision_timestamp", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRevisionTimestamp() {
		return revisionTimestamp;
	}

	public void setRevisionTimestamp(Date revisionTimestamp) {
		this.revisionTimestamp = revisionTimestamp;
	}

	@Column(name = "user_id", nullable = false)
	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	@Column(name = "workspace_id", nullable = false)
	public Integer getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(Integer workspaceId) {
		this.workspaceId = workspaceId;
	}

	@ModifiedEntityNames
	@ElementCollection
	@JoinTable(schema = "audit", name = "revision_info_changed_entities", joinColumns = @JoinColumn(name = "revision_info_id", nullable = false))
	@Column(name = "entity_name", length = 128, nullable = false)
	public Set<String> getChangedEntities() {
		if (changedEntities == null) {
			changedEntities = new HashSet<>();
		}

		return changedEntities;
	}

	public void setChangedEntities(Set<String> changedEntities) {
		this.changedEntities = changedEntities;
	}

	@Override
	public boolean equals(Object value) {
		if (value == this) {
			return true;
		}
		if (!(value instanceof RevisionInfo)) {
			return false;
		}
		RevisionInfo obj = (RevisionInfo) value;

		if (getRevisionNumber() == null || obj.getRevisionNumber() == null) {
			return false;
		}

		return obj.getRevisionNumber().equals(getRevisionNumber());
	}

	@Override
	public int hashCode() {
		int result = 29;
		result += 29 * (getRevisionNumber() != null ? getRevisionNumber().hashCode() : 0);
		return result;
	}

}