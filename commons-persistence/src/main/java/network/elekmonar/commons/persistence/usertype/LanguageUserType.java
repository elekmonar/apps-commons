package network.elekmonar.commons.persistence.usertype;

import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.elefante.hibernate.usertype.EnumUserType;

public class LanguageUserType extends EnumUserType<Language> {

	public LanguageUserType() {
		super(Language.class);
	}
	
	public LanguageUserType(Class<Language> clazz) {
		super(clazz);
	}

}