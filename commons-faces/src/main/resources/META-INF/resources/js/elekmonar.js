 var elekmonar = {

	"session": {
	
		"restoreHandler": function(event) {
			if (event.status == 'success') {
				console.log("restore handler ...");
			}
		},
		
		"restore": function() {
			$.ajax({
			  url: "/classifier/session/restore",
			  context: document.body
			}).done(function() {
				console.log("after ajax ...");
				console.log(this);
				console.log($(this));
			});
		}
	
	},

	"detail": function(e1, e2) {
		console.log("detail");
		console.log(e1);
		console.log(e2);
		return false;
	}

};

$(document).ready(function(){
	$(window).scroll(
		function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		}
	);
});