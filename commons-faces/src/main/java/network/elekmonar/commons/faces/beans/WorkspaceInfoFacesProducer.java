package network.elekmonar.commons.faces.beans;

//import static network.elekmonar.commons.faces.beans.JFWindowConstants.CURRENT_WORKSPACE;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.config.ConfigProperty;

import network.elekmonar.commons.spec.annotations.RuntimeContextSpec;
import network.elekmonar.commons.spec.context.WorkspaceInfoBean;
import network.elekmonar.commons.spec.enums.RuntimeContext;
import network.elekmonar.faces.commons.user.UserBean;

public class WorkspaceInfoFacesProducer implements Serializable {

	private static final long serialVersionUID = 3543775837697727638L;
	
	@Inject @ConfigProperty(name = "default.language", defaultValue = "ru")
	private String defaultLanguage;
	
	@Produces @RuntimeContextSpec(RuntimeContext.FACES)
	public WorkspaceInfoBean produce(UserBean userBean) {
		/*
		Integer workspaceId = (Integer)userBean.getWindowProperty(CURRENT_WORKSPACE);
		
		WorkspaceInfo wi = new WorkspaceInfo();
		wi.setId(workspaceId);		
		*/
		WorkspaceInfoBean wi = new WorkspaceInfoBean();
		wi.setId(1);		
		wi.setLang(defaultLanguage.toUpperCase());
		
		return wi;		
	}
	
}