package network.elekmonar.commons.faces.beans;

public interface JFWindowConstants {

	/**
	 * Текущее пространство
	 */
	public static final String CURRENT_WORKSPACE = "current.workspace";

	/**
	 * Текущий проект
	 */
	public static final String CURRENT_PROJECT = "current.project";

	/**
	 * Целевой проект
	 */
	public static final String TARGET_PROJECT = "target.project";
	
}