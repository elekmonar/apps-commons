package network.elekmonar.commons.faces.beans;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

@RequestScoped
@Named("sessionRestoration")
public class SessionRestoration implements Serializable {

	private static final long serialVersionUID = 3068010201778651927L;
	
	public void restore() {
		System.out.println("try restore session ...");		
		HttpSession httpSession = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		System.out.println("http.session = " + httpSession);
	}

}