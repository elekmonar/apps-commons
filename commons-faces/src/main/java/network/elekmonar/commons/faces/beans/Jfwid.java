package network.elekmonar.commons.faces.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named("jfwid")
public class Jfwid implements Serializable {

	private static final long serialVersionUID = -1280284592837866729L;
	
	private String value;
	
	@PostConstruct
	private void doInit() {
		value = FacesContext.getCurrentInstance().getExternalContext().getClientWindow().getId();
		System.out.println("[doInit] value = " + value);
	}
	
	public String getValue() {
		return value;
	}

}