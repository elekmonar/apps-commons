package network.elekmonar.commons.faces.beans;

import java.io.Serializable;

import network.elekmonar.faces.spi.conversation.LifecycleFlowHandler;

public class ElekmonarLifecycleFlowHandler implements LifecycleFlowHandler, Serializable {

	private static final long serialVersionUID = -664637223106705140L;

	@Override
	public <T> void beforeFinish(T instance, Class<T> entityClass, boolean managed) {
		
	}

}