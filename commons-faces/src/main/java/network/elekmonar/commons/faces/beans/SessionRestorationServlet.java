package network.elekmonar.commons.faces.beans;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/session/restore")
public class SessionRestorationServlet extends HttpServlet {

	private static final long serialVersionUID = 4324448945934104279L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("== try restore session ...");		
		HttpSession httpSession = request.getSession(true);		
		System.out.println("== before.request.session.id = " + request.getRequestedSessionId());
		System.out.println("== http.session = " + httpSession);
		System.out.println("== after.request.session.id = " + request.getRequestedSessionId());		
	}

}